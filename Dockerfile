# Build stage
FROM node:12.4.0-alpine AS builder

WORKDIR /build

COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . .
RUN npm run build

# End builder stage
# Start
FROM node:12.4.0-alpine

WORKDIR /app

COPY --from=builder /build/dist ./dist
COPY --from=builder /build/.env .
COPY --from=builder /build/package.json .
COPY --from=builder /build/package-lock.json .
COPY --from=builder /build/node_modules ./node_modules

CMD [ "npm", "run", "start" ]